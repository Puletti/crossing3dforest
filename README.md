# crossing3dforest
![license](https://img.shields.io/badge/Licence-GPL--3-blue.svg)

An R package to analyse the 3D forest structure (empty spaces) using TLS data.

<img align="right" width="200" height="220" src="/uploads/74708095361e2b72679ebe54573e9926/crossing3dforest_hex_sticker.png">  


----------------------------------------------------------------------------------------


To install the package, try with  
`devtools::install_gitlab("Puletti/crossing3dforest")`  
or  
`devtools::install_gitlab("Puletti/crossing3dforest", build_vignettes = T, upgrade = F)`  
if you whant to authomatically do not install dependencies.  

and then import the library  
`library("crossing3dforest")`  

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

## A short background on point cloud voxelization  
  
<img height="200" src="/uploads/cf53f57bdb5923cb942ebedc2ba655fc/VoxelizationProcess.png">  

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.


## `crossing3dforest` step by step procedure  

`library(tidyverse)`  
`# library(TreeLS)`  suggested, not required  
`library(crossing3dforest)`  


### 1) Import a TLS dataset  
First of all import a normalized TLS point cloud. Please refer to "TreeLS" package for information on Normalization and other preprocessing steps.  

`data("mytls")`  

Check the range of your data:  
`range(mytls@data$X)`  
`range(mytls@data$Y)`  
`range(mytls@data$Z)`  


<img align="right" width="400" height="400" src="/uploads/a32a26d3208b8743e22ff04f56da3ae5/mytls_PointCloudVoxels_050cm.png">  


### 2) Use `VoxFor` function to create a systematic voxel grid.  

The `VoxFor` function computes the number of point in each 1x1x0.25 meters voxel. If the number of points is higher than 10 (see `min.npXvox` argument), the voxel is considered "vegetation", otherwise "empty" (i.e., for example, "available for flight").  
The output of this function is a `data.frame` object that must be used in `df2array` function (see step #3).  


```r
fvg0 <- VoxFor(mytls, 
    minXrect = -10, maxXrect = 10, 
    minYrect = -10, maxYrect = 10, 
    x.vox = .5, y.vox = .5, z.vox = .25,
    min.npXvox = 10, Zcut = 3)
    
fvg0 %>% head

```




### 3) Transform the `fvg0 data.frame` in an specific array

The `df2array` function requires the `data.frame` obtained from `VoxFor` function and the minimum and maximum values of Z layer of interest.  
  
In the example below, we opted to obtain information for the layer ranging between 3 and 5 meters in Z (`z.min = 3, z.max = 5`).   


```r
arr_from3_to5 <- fvg0 %>% df2array(z.min = 3, z.max = 5)
```

> Remember: the point in input was already normalized (see step #1 at the top of the page)

The output is a list of two elements.  
The first element of the output list is the exact array that will be used in `percolation_sts` function (see step #4). 


```r
arr_from3_to5[[1]][, , 1:3]
```

The latter element can be used for graphical functions (see other details).

```r
arr_from3_to5[[2]]
```


### 4) Percolation function  

```r
perc_3_5 <- percolation_sts(arr_from3_to5[[1]])
```


The first element in the output-list is a `tibble` containing information in each direction:  
X (dim_num >> 1);  
Y (dim_num >> 2);  
Z (dim_num >> 3).  

The column `pc_lbl` is a unique id-code associated with percolating cluster and the last column (`pc_min_sect`) represents the minimum number of voxels of lowest section found in crossing the specific direction (i.e. bottleneck).  

```r
perc_3_5[[1]]
```


The second element in the output-list is a `tibble` with, again, the `pc_lbl` code and, in the second column, the number of cells in such cluster.  


```r
perc_3_5[[2]]
```

# Graphics with `crossing3dforest`

The `plotVoxel` function allows adding classified voxels (“empty” or “vegetation”) to an `rgl` plot. Graphical function parameters can be profitably obtained using the values obtained from functions described here above.

![3dplot](/uploads/b1684588ad19d1d23e938ffd97b75265/3dplot.png)

